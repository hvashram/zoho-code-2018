calendar Departing_Calendar_New
{
	displayName = "_Departing_Calendar_New"
	show all rows from Reservation_Form  [(Room_Status != "Booked" && Room_Status != "Cancelled" && To_Date is today || To_Date in next 21 days) || (Room_Status == "Checked Out" && To_Date in last 45 days)]  
	(
		Flagged_New
		Resa_Image
		Title
		Guest_First_Name as "First Name"
		Suggested_Donation_Breakdown2
		Arrival_Time as "Arrival Time"
		Departure_Time as "Departure Time"
		View_Room_Map as "View Room Map for these dates (opens in new window)"
		Show_all_rooms as "Show all rooms"
		Standard_Email as "Standard Email"
		Suggested_Donation as "Suggested Donation"
		Suggested_Donation_Last_Updated as "Suggested Donation Last Updated"
		Donation_Received as "Donation Received"
		Balance_Due as "Balance Due"
		Deposit
		Donation
		RoomData
		Room_is_Available
		Long_Term_Stay
		Res_Name
		TotDayResName
		Room_From_To
		TotDaysRemainName
		Conducted_Program_List_Name
		Number_of_Days1 as "Number of days this year"
		Number_of_Total_Days as "Number of total days"
		Long_Term_Reservation
		Force_Sync_Donation as "Force Sync Donation"
		Force_Sync_Donation_Timestamp
		Retreat
		Same_Day
		Check_Room1 as "Check_Room"
		Assigned_Counselor as "Assigned Counselor"
		Dept_Assigned
		Authorized_Driver as "Authorized Driver"
		Guest_Remarks as "Guest Remarks"
		Attending_CP as "Is this Reservation for a Conducted Program?"
		Conducted_Programs_Info as "Conducted Program"
		Program_attendance_status as "Conducted Program Status"
		Waiting_Priority as "Waiting Priority "
		Preferences
		Duplicate_room as "Duplicate room"
		add_manual_donation as "Manually add or check donation for this reservation"
		Reservation_Notes as "Reservation Notes"
		Communicate_On_GuestPlan as "Note to Guest. (Shows up on envelope)"
		Reservation_History as "Reservation History"
		Conducted_Program_Amt1 as "Conducted Program Retreat"
		CP_Deposit
		Donation_Amount as "Donation Amount"
		Balance_Due_CP as "Balance_Due CP"
		DateTaken
		Initial
		Final
		Before
		After
		Overall_Start as "Overall Start"
		Overall_End as "Overall End"
		Get_Year as "Get_Year_Arriving"
		Get_Month as "Get_Month_Arriving"
		Days_Remaining
		Guest_Last_Name as "Last Name"
		Check_Room as "OK to Work"
		Guest_Type as "Guest Type"
		Res_Name2
		Guest_Info as "Guest Info"
		Image
		Email
		To_Date as "To Date"
		RoomNbr as "Room Number"
		From_Date as "From Date"
		FirstLast
		Room_Status as "Room Status"
	)
	options
	(
		display type = month
		week start day = monday
		display field = RoomData
		start date = To_Date
		default date 
		(
	    	day   = Today
	    	month = Currentmonth
	    	year  = Currentyear
		)
	)
	conditional formatting
	(
		"Checking Out Next 3 Days"
		{
			condition = (To_Date in next 3 days || To_Date is today)
			format = "color:#ffffff;background-color:#e84c3d;"
		}
		"Checked Out B4 Today"
		{
			condition = (To_Date < today)
			format = "color:#cbcbcb;background-color:#fce7e7;"
		}
		"Cancelled"
		{
			condition = (Room_Status == "Cancelled")
			format = "color:#ffffff;background-color:#c516ef;"
		}
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					RoomNbr as "Room Number"
					FirstLast
					From_Date as "From Date"
					To_Date as "To Date"
					Room_Status as "Room Status"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Check_Room as "OK to Work"
					Guest_Info as "Guest Info"
					Flagged_New
					Resa_Image
					Title
					Guest_First_Name as "First Name"
					Guest_Last_Name as "Last Name"
					Email
					Suggested_Donation_Breakdown2
					From_Date as "From Date"
					Arrival_Time as "Arrival Time"
					To_Date as "To Date"
					Departure_Time as "Departure Time"
					View_Room_Map as "View Room Map for these dates (opens in new window)"
					Guest_Type as "Guest Type"
					Show_all_rooms as "Show all rooms"
					RoomNbr as "Room Number"
					Standard_Email as "Standard Email"
					Suggested_Donation as "Suggested Donation"
					Suggested_Donation_Last_Updated as "Suggested Donation Last Updated"
					Donation_Received as "Donation Received"
					Balance_Due as "Balance Due"
					Deposit
					Donation
					RoomData
					Room_is_Available
					Long_Term_Stay
					Res_Name
					TotDayResName
					Room_From_To
					TotDaysRemainName
					Conducted_Program_List_Name
					Number_of_Days1 as "Number of days this year"
					Number_of_Total_Days as "Number of total days"
					Long_Term_Reservation
					Force_Sync_Donation as "Force Sync Donation"
					Force_Sync_Donation_Timestamp
					Retreat
					Same_Day
					Check_Room1 as "Check_Room"
					FirstLast
					Room_Status as "Room Status"
					Assigned_Counselor as "Assigned Counselor"
					Dept_Assigned
					Authorized_Driver as "Authorized Driver"
					Guest_Remarks as "Guest Remarks"
					Attending_CP as "Is this Reservation for a Conducted Program?"
					Conducted_Programs_Info as "Conducted Program"
					Program_attendance_status as "Conducted Program Status"
					Waiting_Priority as "Waiting Priority "
					Preferences
					Duplicate_room as "Duplicate room"
					add_manual_donation as "Manually add or check donation for this reservation"
					Reservation_Notes as "Reservation Notes"
					Communicate_On_GuestPlan as "Note to Guest. (Shows up on envelope)"
					Reservation_History as "Reservation History"
					Conducted_Program_Amt1 as "Conducted Program Retreat"
					CP_Deposit
					Donation_Amount as "Donation Amount"
					Balance_Due_CP as "Balance_Due CP"
					Image
					DateTaken
					Initial
					Final
					Before
					After
					Overall_Start as "Overall Start"
					Overall_End as "Overall End"
					Get_Year as "Get_Year_Arriving"
					Get_Month as "Get_Month_Arriving"
					Days_Remaining
					Res_Name2
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    	 	)
    	)
	)
}
