form Room_Availability_2016
{
	displayname = "2016 Room Availability"
	field alignment = left
	feature property = 32
	Get_Month_Arriving
	(
		type = number
		maxchar = 99
	 	row = 1
	 	column = 1   
		width = 100px
	)
	Get_Year_Arriving
	(
		type = number
		maxchar = 99
	 	row = 1
	 	column = 1   
		width = 100px
	)
	Donation_Received
	(
		type = USD
		displayname = "Donation Received"
		maxchar = 99
	 	row = 1
	 	column = 1   
		width = 100px
	)
	First_Name
	(
    	type = text
		displayname = "First Name"
	 	row = 1
	 	column = 1   
		width = 200px
	)
	Last_Name
	(
    	type = text
		displayname = "Last Name"
	 	row = 1
	 	column = 1   
		width = 200px
	)
	Guest_Type
	(
    	type = text
		displayname = "Guest Type"
	 	row = 1
	 	column = 1   
		width = 200px
	)
	Guest_Status
	(
		type = picklist
		displayname = "Guest Status"
		values = {"Monk", "Resident", "-NONE-", "Volunteer", "Donor", "Employee", "Cave Dweller"}
	 	row = 1
	 	column = 1   
		width = 206px
	)
	Room
	(
		type = picklist
		values = {"040", "017", "024", "North Cottage 2", "North Cottage 1", "039", "056", "106", "010", "018", "005", "108", "003", "006", "ML-2", "102", "004", "023", "103", "107", "021", "022", "Sadhana Kutir", "105", "101", "041", "109", "104", "054", "033", "050", "001", "019", "047", "020", "043", "051", "034", "038", "052", "042", "BH-3", "055", "BH-4", "045", "053", "048", "007", "049", "Volunteers", "035", "002", "ML-1", "Employee", "011", "046", "012", "BH-2", "008", "028", "027", "009", "ML-3", "026", "025", "044", "014", "015", "030", "032", "031", "Grny1", "North Cottage 4", "016", "037", "036", "Grny2"}
	 	row = 1
	 	column = 1   
		width = 206px
	)
	Room_Status
	(
		type = picklist
		displayname = "Room Status"
		values = {"Checked Out", "Occupied", "No Show", "Booked", "Maintenance", "Blank"}
	 	row = 1
	 	column = 1   
		width = 206px
	)
	TotDays
	(
		type = number
		maxchar = 99
	 	row = 1
	 	column = 1   
		width = 100px
	)
	Arriving
	(
    	type = date
	 	row = 1
	 	column = 1   
		width = 130px
	)
	Departing
	(
    	type = date
	 	row = 1
	 	column = 1   
		width = 130px
	)
	CP
	(
    	type = checkbox
		displayname = "CP?"
		initial value = false
	 	row = 1
	 	column = 1   
		width = 206px
	)
	
	actions
	{
		on add
		{
			submit
			(
   				type = submit
   				displayname = "Submit"
			)
			reset
			(
   				type = reset
   				displayname = "Reset"
			)
		}
		on edit
		{
			update
			(
   				type = submit
   				displayname = "Update"
			)
			cancel
			(
   				type = cancel
   				displayname = "Cancel"
			)
		}
	}
}
