form Add_Cash_or_Check_Deposit
{
	displayname = "Add Cash or Check Deposit"
	success message = "Data Added Successfully!"
	field alignment = left
	feature property = 32
			hide = true
	Guest
	(
		type = picklist
		values  = Guest_Info_Form.ID
    	displayformat = [Full_Name1]
		searchable = true
		sortorder = ascending
	 	row = 1
	 	column = 1   
		width = 206px
	)
	must have Reservation
	(
		type = picklist
		values  = Reservation_Form.ID
    	displayformat = [Guest_First_Name + " " + Guest_Last_Name + ", " + From_Date + "- " + To_Date]
		searchable = true
		sortorder = ascending
	 	row = 1
	 	column = 1   
		width = large
	)
	must have Donation_Amount
	(
		type = decimal
		displayname = "Donation Amount"
	 	row = 1
	 	column = 1   
		width = 100px
	)
	must have Payment_Type
	(
		type = picklist
		displayname = "Payment Type"
		values = {"Cash", "Check"}
	 	row = 1
	 	column = 1   
		width = 206px
	)
	
	actions
	{
		on add
		{
			submit
			(
   				type = submit
   				displayname = "Submit"
			)
			reset
			(
   				type = reset
   				displayname = "Reset"
			)
		}
		on edit
		{
			update
			(
   				type = submit
   				displayname = "Update"
			)
			cancel
			(
   				type = cancel
   				displayname = "Cancel"
			)
		}
	}
}
