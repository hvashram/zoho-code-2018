form Assign_Ashram_Duties
{
	displayname = "Assign Ashram Duties"
	success message = "Ashram Duty Assigned Successfully"
	field alignment = left
	Section
	(
		type = section
	 	row = 1
	 	column = 0   
		width = medium
	)
	Duties_List
	(
		type = picklist
		displayname = "Duties List"
		values  = Duties_List.ID
    	displayformat = [Duty_Name]
		searchable = true
		sortorder = ascending
	 	row = 1
	 	column = 1   
		width = medium
	)
	Assign_To
	(
		type = picklist
		displayname = "Assign To"
		values  = Reservation_Form[(Guest_Type == "Resident" || Guest_Type == "Long Term Resident" && Room_Status == "Occupied") || (Guest_Type == "Monk" && Room_Status == "Confirmed") || (Guest_Type == "Monk" && Room_Status == "Occupied") || (Guest_Type == "Resident Monastic" && Room_Status == "Occupied")].ID
    	displayformat = [FirstLast + "[ " + Days_Remaining + " }  " + RoomData]
		sortorder = ascending
	 	row = 1
	 	column = 1   
		width = medium
	)
	Guest_Type
	(
    	type = text
		displayname = "Guest Type"
	 	row = 1
	 	column = 1   
		width = medium
	)
	Start_Date
	(
    	type = date
		displayname = "Start Date"
		alloweddays = 0,1,2,3,4,5,6
	 	row = 1
	 	column = 1   
		width = medium
	)
	End_Date
	(
    	type = date
		displayname = "End Date"
		alloweddays = 0,1,2,3,4,5,6
	 	row = 1
	 	column = 1   
		width = medium
	)
	Start_Date_Time
	(
    	type = datetime
		displayname = "Start Date Time"
		alloweddays = 0,1,2,3,4,5,6
	 	row = 1
	 	column = 1   
		width = medium
	)
	End_Date_Time
	(
    	type = datetime
		displayname = "End Date Time"
		alloweddays = 0,1,2,3,4,5,6
	 	row = 1
	 	column = 1   
		width = medium
	)
	Duration
	(
    	type = formula
		value =  (input.End_Date_Time - input.Start_Date_Time)
		visibility = false
	 	row = 1
	 	column = 1   
		width = medium
	)
	
	actions
	{
		on add
		{
			submit
			(
   				type = submit
   				displayname = "Assign"
			)
			reset
			(
   				type = reset
   				displayname = "Reset"
			)
		}
		on edit
		{
			update
			(
   				type = submit
   				displayname = "Update"
			)
			cancel
			(
   				type = cancel
   				displayname = "Cancel"
			)
		}
	}
}
