form Resident_Form_Templates
{
	displayname = "Resident Form Templates"
	success message = "Data Added Successfully!"
	field alignment = left
	Section
	(
		type = section
	 	row = 1
	 	column = 0   
		width = medium
	)
	Form_Name
	(
    	type = text
		displayname = "Form Name"
	 	row = 1
	 	column = 1   
		width = medium
	)
	FormText
	(
    	type = richtext
		height = 200px
		toolbar = enable
		[
			style, font-color, background-color, alignment, link, table, bullets-and-numbering, 
			font-size : {1, 2, 3, 4, 5, 6, 7},
			font-family : {"Serif", "Arial", "Courier New", "Georgia", "Tahoma", "Times New Roman", "Trebuchet", "Verdana", "Comic Sans Ms"}
		]
	 	row = 1
	 	column = 1   
		width = medium
	)
	
	actions
	{
		on add
		{
			submit
			(
   				type = submit
   				displayname = "Submit"
			)
			reset
			(
   				type = reset
   				displayname = "Reset"
			)
		}
		on edit
		{
			update
			(
   				type = submit
   				displayname = "Update"
			)
			cancel
			(
   				type = cancel
   				displayname = "Cancel"
			)
		}
	}
}
