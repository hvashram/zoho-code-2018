form State_Add
{
	field alignment = left
	feature property = 32
	State_Name
	(
    	type = text
	 	row = 1
	 	column = 1   
		width = 200px
	)
	State_Code
	(
    	type = text
	 	row = 1
	 	column = 1   
		width = 200px
	)
	Country_Name_State
	(
		type = picklist
		values  = Country_Enter.ID
    	displayformat = [Name_Country]
		searchable = true
		sortorder = ascending
	 	row = 1
	 	column = 1   
		width = 206px
	)
	
	actions
	{
		on add
		{
			submit
			(
   				type = submit
   				displayname = "Submit"
			)
			reset
			(
   				type = reset
   				displayname = "Reset"
			)
		}
		on edit
		{
			update
			(
   				type = submit
   				displayname = "Update"
			)
			cancel
			(
   				type = cancel
   				displayname = "Cancel"
			)
		}
	}
}
