list conductedPrograms.getConductedProgramGuestAndWaitlist(int conducted_program_ID)
{
	//Returns all reservations that are "Approved" and "Waiting" for a conducted program
	//The Approved guests come first, while the Waiting guests are ordered according to their
	//positions on the waitlist
	conducted_program = Conducted_Programs_Info_Form[ID == input.conducted_program_ID];
	all_res = {};
	for each  res in Reservation_Form[Conducted_Programs_Info == conducted_program.ID && Program_attendance_status == "Approved" || Program_attendance_status == "Attended"] sort by Added_Time
	{
		all_res.add(res.ID);
	}
	for each  res in Reservation_Form[Conducted_Programs_Info == conducted_program.ID && Program_attendance_status == "Waiting" && Waiting_Priority is not null] sort by Waiting_Priority
	{
		all_res.add(res.ID);
	}
	for each  res in Reservation_Form[Conducted_Programs_Info == conducted_program.ID && Program_attendance_status == "Waiting" && Waiting_Priority is null] sort by Added_Time
	{
		all_res.add(res.ID);
	}
	return all_res;
}