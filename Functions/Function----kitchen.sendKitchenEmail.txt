bool kitchen.sendKitchenEmail()
{
	email = Standard_Email[Email_Type == "Z - Kitchen Conducted Program Notification"];
	//"recipient" is a dud reservation; it's really just being used as a structure
	//to send in the conducted program information. Bad form, but it keeps
	//with the rest of the email data structures I've made. Might need to be
	//changed in the future to keep up with best form, but meh...
	//Replace tags in the email subject line and message body
	recipient = thisapp.stdEmail.getEmailAddress("Kitchen");
	message_subject = email.Subject_field;
	message_text = email.Message_field;
	tags = thisapp.stdEmail.getStandardEmailTagMap(Reservation_Form[ID is null]);
	message_text = thisapp.stdEmail.replaceEmailTags(message_text,tags);
	message_subject = thisapp.stdEmail.replaceEmailTags(message_subject,tags);
	//Send the email
	return thisapp.stdEmail.sendHVAshramEmail(recipient,message_subject,message_text);
}