bool payments.depositRequired(int reservationID)
{
	// *** Accepts a Reservation ID and determines if a Deposit is required or not ***
	depositRequired = false;
	if(!isNull(reservationID) && input.reservationID > 0)
	{
		reservationRecord = Reservation_Form[ID == input.reservationID];
		if(!isNull(reservationRecord.Deposit) && !isBlank(reservationRecord.Deposit) && reservationRecord.Deposit != "N/A")
		{
			depositRequired = true;
		}
	}
	return depositRequired;
}