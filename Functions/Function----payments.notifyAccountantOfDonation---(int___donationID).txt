void payments.notifyAccountantOfDonation(int donationID)
{
	// *** Accepts a Donation record ID and notifies the accountant (Br. Roy) of donations of $250 or greater, so he can create a TPL file ***
	donationRecord = Donation[ID == input.donationID];
	// Only proceed if the donation amount is $250 or greater, and if the accountant has not already been notified
	if(donationRecord.Donation_Amount >= 250 && isNull(donationRecord.Accountant_Notified))
	{
		// If it's a PayPal donation, we don't actually need to send a notification, since Br. Roy does them all in large batches, rather than one-by-one //2018-06-20 Francis changed this from != "PayPal" to =  "PayPal" || donationRecord.Payment_Method ="Credit Card" || donationRecord.Payment_Method ="Cash"  remarked out this line to see if we CAN generate accountant notification for PayPal donations
		if(donationRecord.Payment_Method != "PayPal")
		{
			// Lookup the guest name, unless this is an anonymous donation
			guestName = "Anonymous";
			if(!isNull(donationRecord.Guest))
			{
				guestRecord = Guest_Info_Form[ID == donationRecord.Guest];
				guestName = thisapp.str.capitalizeFirstLetter(guestRecord.First_Name) + " " + thisapp.str.capitalizeFirstLetter(guestRecord.Last_Name);
			}
			else if(!isNull(donationRecord.First_Name) && !isNull(donationRecord.Last_Name))
			{
				guestName = thisapp.str.capitalizeFirstLetter(donationRecord.First_Name) + " " + thisapp.str.capitalizeFirstLetter(donationRecord.Last_Name);
			}
			// Create the HTML link we'll insert into the e-mail (link opens a form for viewing the donation info and adding the TPL)
			formURL = "https://creator.zoho.com" + zoho.appuri + "#Form:Donation_TPL?Donation=" + donationRecord.ID;
			formLink = "<a href='" + formURL + "' style='font-size:1.5em;'>View Donation Details and Upload TPL File</a>";
			// Email subject
			messageSubject = "A donation has been received: $" + donationRecord.Donation_Amount.toLong() + " (" + guestName + ")";
			// Email body
			messageBody = "A donation has been received.";
			messageBody = messageBody + "<br/><br/><u>Donation Amount</u>: $" + donationRecord.Donation_Amount.toLong();
			messageBody = messageBody + "<br/><u>Payment Method</u>: " + donationRecord.Payment_Method;
			messageBody = messageBody + "<br/><u>Received From</u>: " + guestName;
			messageBody = messageBody + "<br/><u>Received</u>: " + donationRecord.Date_Received;
			messageBody = messageBody + "<br/><u>Recorded By Clerk</u>: " + donationRecord.Added_Time;
			messageBody = messageBody + "<br/><br/>Please click the link below to view the details of this donation:";
			messageBody = messageBody + "<br/><br/>" + formLink;
			messageBody = messageBody + "<br/><br/><b>(<span style='color:red;'>*</span>) After creating the TPL file, please enter the TPL number and upload the TPL file using the link above.</b>";
			// Send the e-mail notification to the accountant
			sendmail
			[
				from :zoho.adminuserid
				to :"Br. Roy <roy@hvashram.org>"
				subject :messageSubject
				message :messageBody
			]
		}
		// Record the date/time that the notification to the accountant was sent
		donationRecord.Accountant_Notified=now;
		// Check if any vital info is missing from the Guest record, and notify an admin if so (since we need e-mail, etc. for the TPL)
		thisapp.guest.checkForMissingVitalInfo(donationRecord.Guest);
	}
}