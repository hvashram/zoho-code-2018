void res.linkConnectedReservations(int reservationID)
{
	// *** Accepts a Reservation ID and searches for any connected reservations immediately before or after this reservation ***
	// *** (e.g. when a resident changes rooms, and a new Reservation record is created for his new room) ***
	// *** The ID's of immediately connected reservations go in the "Before" and "After" fields ***
	// *** The ID's of the initial and final reservations in the series go in "Initial" and "Final" respectively ***
	// *** "Overall_Start" and "Overall_End" hold the overall start and end dates of the entire reservation, spanning multiple records ***
	thisReservation = Reservation_Form[ID == input.reservationID];
	// Only link Resident reservation records
	if(thisReservation.count() > 0 && thisReservation.Guest_Type == "Resident")
	{
		// --- Reset the Initial, Final, Before, and After fields on this reservation to NULL before beginning
		thisReservation.Initial=null;
		thisReservation.Final=null;
		thisReservation.Before=null;
		thisReservation.After=null;
		// --- Search for connected Reservation record(s) immediately before this one
		currentRecord = Reservation_Form[ID == thisReservation.ID];
		for each  beforeRecord in Reservation_Form[Guest_Info == thisReservation.Guest_Info && Guest_Type == thisReservation.Guest_Type && To_Date <= thisReservation.From_Date && Room_Status != "Cancelled"] sort by To_Date desc
		{
			if(beforeRecord.To_Date >= currentRecord.From_Date.subDay(7))
			{
				// Reset the Initial, Final, Before, and After fields on the "before" record
				beforeRecord.Initial=null;
				beforeRecord.Final=null;
				beforeRecord.Before=null;
				beforeRecord.After=null;
				// Link the current reservation record with the one before it
				currentRecord.Before=beforeRecord.ID;
				beforeRecord.After=currentRecord.ID;
				// The "Initial" record in the series has shifted to the record before the current one
				currentRecord.Initial=beforeRecord.ID;
				// Tell the other records in the series that the "Initial" record has shifted 
				for each  recordToBeUpdated in Reservation_Form[Initial == currentRecord.ID]
				{
					recordToBeUpdated.Initial=currentRecord.Initial;
				}
				// Shift the "current" record back one
				currentRecord = Reservation_Form[ID == beforeRecord.ID];
			}
		}
		// --- Search for connected Reservation record(s) immediately after this one
		currentRecord = Reservation_Form[ID == thisReservation.ID];
		for each  afterRecord in Reservation_Form[Guest_Info == thisReservation.Guest_Info && Guest_Type == thisReservation.Guest_Type && From_Date >= thisReservation.To_Date && Room_Status != "Cancelled"] sort by From_Date
		{
			if(afterRecord.From_Date <= currentRecord.To_Date.addDay(7))
			{
				// Reset the Initial, Final, Before, and After fields on the "after" record
				afterRecord.Initial=null;
				afterRecord.Final=null;
				afterRecord.Before=null;
				afterRecord.After=null;
				// Link the current reservation record with the one after it
				currentRecord.After=afterRecord.ID;
				afterRecord.Before=currentRecord.ID;
				// The "Final" record in the series has shifted to the record after the current one
				currentRecord.Final=afterRecord.ID;
				// Tell the other records in the series that the "Final" record has shifted 
				for each  recordToBeUpdated in Reservation_Form[Final == currentRecord.ID]
				{
					recordToBeUpdated.Final=currentRecord.Final;
				}
				// Shift the "current" record forward one
				currentRecord = Reservation_Form[ID == afterRecord.ID];
			}
		}
		// --- Refresh the "thisReservation" object (to ensure it reflects the latest data from the database)
		thisReservation = Reservation_Form[ID == input.reservationID];
		// --- Determine the "Initial" and "Final" records in this series
		initialRecordID = if(!isNull(thisReservation.Initial) && thisReservation.Initial > 0,thisReservation.Initial,thisReservation.ID);
		finalRecordID = if(!isNull(thisReservation.Final) && thisReservation.Final > 0,thisReservation.Final,thisReservation.ID);
		// --- Apply the correct "Initial" value to all records in this series
		for each  reservationRecord in Reservation_Form[ID == finalRecordID || Final == finalRecordID && ID != initialRecordID]
		{
			reservationRecord.Initial=initialRecordID;
		}
		// --- Apply the correct "Final" value to all records in this series
		for each  reservationRecord in Reservation_Form[ID == initialRecordID || Initial == initialRecordID && ID != finalRecordID]
		{
			reservationRecord.Final=finalRecordID;
		}
		// --- Determine & set the overall start date of the entire reservation spanning multiple records
		// Determine the overall start date
		if(initialRecordID != thisReservation.ID)
		{
			// Get the overall start date from the Initial record in the series
			overallStart = Reservation_Form[ID == initialRecordID].From_Date;
		}
		else
		{
			// This reservation is the Initial record in the series, so get the overall start date from this reservation
			overallStart = thisReservation.From_Date;
		}
		// Set the Overall_Start date on all records in this series
		for each  reservationRecord in Reservation_Form[ID == initialRecordID || Initial == initialRecordID]
		{
			reservationRecord.Overall_Start=overallStart;
		}
		// --- Determine & set the overall end date of the entire reservation spanning multiple records
		// Determine the overall end date
		if(finalRecordID != thisReservation.ID)
		{
			// Get the overall end date from the Final record in the series
			overallEnd = Reservation_Form[ID == finalRecordID].To_Date;
		}
		else
		{
			// This reservation is the Final record in the series, so get the overall end date from this reservation
			overallEnd = thisReservation.To_Date;
		}
		// Set the Overall_End date on all records in this series
		for each  reservationRecord in Reservation_Form[ID == finalRecordID || Final == finalRecordID]
		{
			reservationRecord.Overall_End=overallEnd;
		}
	}
}