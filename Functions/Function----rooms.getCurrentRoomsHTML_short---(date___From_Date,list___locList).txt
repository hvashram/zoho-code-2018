string rooms.getCurrentRoomsHTML_short(date From_Date, list locList)
{
	//Returns a formatted HTML table of current rooms and their occupants
	From_Date = ifnull(From_Date,now);
	// 	roomList = Room_Inventory[ID != 0].ID.getAll();
	roomList = Room_Inventory[ID != 0].ID.getAll();
	// info roomList;
	//if (((input.locList.toString()  ==  null)  ||  (input.locList.size()  <=  0))  ||  (input.locList.toString()  ==  ""))
	//{
	locList = thisapp.rooms.getDefaultMapRoomsList();
	//}
	URL = "https://app.zohocreator.com" + zoho.appuri + "Reservation_Form/record-edit/Reservations_View_Edit/";
	dayList = List:Int({0,1,2,3,4,5});
	numberDaysToDisplay = 5;
	HTML = "<table><thead><tr><th>&nbsp;</th>";
	for each  d in dayList
	{
		HTML = HTML + "<th>" + From_Date.addDay(d) + "</th>";
	}
	HTML = HTML + "</tr></thead>";
	for each  loc in locList
	{
		HTML = HTML + "<tr><th colspan=\"" + numberDaysToDisplay + 1 + "\">" + loc + "</th></tr>";
		for each  room in Room_Inventory[Room_Location == loc] sort by Room_Number
		{
			HTML = HTML + "<tr><td>" + room.Room_Number + "</td>\n";
			for each  day in dayList
			{
				current_day = From_Date.addDay(day);
				resa = Reservation_Form[Room_Status != "Cancelled" || Room_Status == "Confirmed" || Room_Status == "Occupied" || Room_Status == "Checked Out" && RoomNbr == room.ID && From_Date <= current_day && To_Date >= current_day];
				Guest = "";
				for each  r in resa
				{
					if(Guest != "")
					{
						//Denotes multiple guests in a room on same day. Red for emphasis;
						//housekeeping needs to see them.
						Guest = Guest + " <font color=\"red\"><b>////SAME DAY////</b></font> ";
					}
					Guest = Guest + "<a " + if(r.Room_Status == null,"","class=\"" + r.Room_Status + "\"") + " href=\"" + URL + r.ID + "\" target=\"_blank\">" + Guest_Info_Form[ID == r.Guest_Info].Full_Name1 + "</a>";
				}
				//Commented out code for putting only one guest name per box.
				/*if (resa.Guest_Info  !=  null)
                {
                    Guest = "<a " + if((resa.Room_Status  ==  null),"","class=\"" + resa.Room_Status + "\"") + " href=\"" + URL + resa.ID + "\">";
                    Guest = Guest + Guest_Info_Form[ID == resa.Guest_Info].Full_Name1;
                    Guest = Guest + "</a>";
                }*/
				DATE_STRING = "(" + resa.From_Date + " - " + resa.To_Date + ")";
				// If From_Date is Today, background cell in Green
				td_class = "";
				if(resa.From_Date == zoho.currentdate)
				{
					td_class = "style='background-color:#64FE88;'";
				}
				HTML = HTML + "<td " + td_class + if(resa.Room_Status == null,"","class=\"" + resa.Room_Status + "\"") + ">" + if(Guest == "","",Guest + "<br><span class=\"showdate\">" + DATE_STRING + "</span>") + "</td>";
			}
			HTML = HTML + "</tr>";
		}
	}
	HTML = HTML + "</table>" + "\n" + "<style>" + "table, th, td {border: 1px solid black;}" + "table { page-break-inside:auto }" + "tr { page-break-inside:avoid; page-break-after:auto }" + "thead { display:table-header-group }" + "tfoot { display:table-footer-group }" + "td, th { font-family:tahoma; font-size:18px; text-align:center; border-width:1px;}" + "a {text-decoration: none;}" + ".tdSansBords { border-right:none; border-left:none;}" + ".Confirmed {color:black; font-weight: bold;}" + ".Occupied {color:blue;}" + ".showdate {font-size:18px;}" + "</style>";
	return HTML;
}