string str.decode(string strToDecode)
{
	// This salt active since: 2015-11-07 13:25
	// salt = "riZEZ|Rt%o975.m~*7K/0,O2OsAeNE";
	// strToDecodeWithSalt = strToDecode+ ":" + salt;
	decodedString = zoho.encryption.base64Decode(strToDecode);
	return decodedString;
}