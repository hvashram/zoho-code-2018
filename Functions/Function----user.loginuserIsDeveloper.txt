bool user.loginuserIsDeveloper()
{
	// *** Returns TRUE if the currently logged-in user is a Developer, FALSE if not ***
	return thisapp.user.loginuserHasRole("Developer");
}