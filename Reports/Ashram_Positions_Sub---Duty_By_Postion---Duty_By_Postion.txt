list Duty_By_Postion
{
	displayName = "Duty_By_Postion"
   	hide = true
	show all rows from Ashram_Positions_Sub    
	(
		Assigned_To.FirstLast
		Ashram_Depts_Locations1.Position
		Start_Date
		Start_Time
		DayOfWeek
		DayOfWeek2
		End_Date
		End_Time
		Ashram_Depts_Locations1 as "Ashram Depts / Locations"
		Ashram_Depts_Locations1.Department_Name as "Department Name"
		Ashram_Depts_Locations1.Location
		Ashram_Depts_Locations1.Training
		Duration
		Assigned_To as "Assigned To"
	)
	filters
	(
		Ashram_Depts_Locations1
		Assigned_To
		Start_Date
		Ashram_Quarterly_Duty_Schedule
		"Mondays"  :  DayOfWeek == "Monday"
		"Tuesdays"  :  DayOfWeek == "Tuesday"
		"Wednesdays"  :  DayOfWeek == "Wednesday"
		"Thursdays"  :  DayOfWeek == "Thursday"
		"Fridays"  :  DayOfWeek == "Friday"
		"Saturdays"  :  DayOfWeek == "Saturday"
		"Sundays"  :  DayOfWeek == "Sunday"
	)
	group by
	(
		Ashram_Depts_Locations1.Position ascending
	)
	sort by
	(
		Start_Date ascending
	)
	conditional formatting
	(
		"Expired"
		{
			condition = (Start_Date < today)
			fields = [Ashram_Depts_Locations1.Position, Assigned_To.FirstLast, Start_Date, End_Date, Start_Time, End_Time]
			format = "text-decoration:line-through;color:#bdbdbd;"
		}
		"Skimming"
		{
			condition = (Ashram_Depts_Locations1.Position.contains("Skimming") && Start_Date_Time > today)
			fields = [Ashram_Depts_Locations1.Position, Assigned_To.FirstLast]
			format = "font-weight:bold;color:#1e11d4;"
		}
		"Lockup"
		{
			condition = (Ashram_Depts_Locations1.Position.contains("Lock") && Start_Date_Time > today)
			fields = [Ashram_Depts_Locations1.Position, Assigned_To.FirstLast]
			format = "font-weight:bold;"
		}
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Assigned_To.FirstLast as "FirstLast"
					Ashram_Depts_Locations1.Position as "Position"
					Start_Date
					Start_Time
					DayOfWeek
					DayOfWeek2
					End_Date
					End_Time
					Ashram_Depts_Locations1 as "Ashram Depts / Locations"
					Ashram_Depts_Locations1.Department_Name as "Department Name"
					Ashram_Depts_Locations1.Location as "Location"
					Ashram_Depts_Locations1.Training as "Training"
					Duration
					Assigned_To as "Assigned To"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Add 
    	 	)
    	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Assigned_To.FirstLast as "FirstLast"
					Ashram_Depts_Locations1.Position as "Position"
					Start_Date
					Start_Time
					DayOfWeek
					DayOfWeek2
					End_Date
					End_Time
					Ashram_Depts_Locations1 as "Ashram Depts / Locations"
					Ashram_Depts_Locations1.Department_Name as "Department Name"
					Ashram_Depts_Locations1.Location as "Location"
					Ashram_Depts_Locations1.Training as "Training"
					Duration
					Assigned_To as "Assigned To"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Print 
    	 	)
    	)
	)
}
