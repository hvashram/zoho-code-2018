list Donations_PayPal
{
	displayName = "Donations (PayPal)"
	show all rows from Donation  [Payment_Status == "Complete" && Payment_Method == "PayPal" && PayPal_Transferred_To_Bank is null]  
	(
		Force_Sync_Reservation as "Force Sync Reservation"
		Force_Sync_Reservation_Timestamp
		ID
		Donation_Type as "Donation Type"
		(
			width = 0px
		)
		Fund
		Donation_Amount as "Donation Amount"
		(
			width = 0px
			display = total	
		)
		Transaction_Fee as "Transaction Fee"
		(
			display = total	
		)
		Amount_Received as "Amount Received"
		(
			width = 0px
			display = total	
		)
		Percentage
		Payment_Method as "Payment Method"
		Guest
		(
			width = 183px
			view = Guest_Info_Rpt
		)
		Reservation
		(
			width = 235px
			view = Reservations_View_Edit
		)
		Conducted_Program as "Conducted Program"
		(
			view = CP_CurrentYear
		)
		Transaction_Number as "Transaction Number"
		(
			width = 161px
		)
		ZC_Payment.payment_id as "PayPal ID"
		(
			view = Paypal_WPS_History_View
		)
		Date_Received as "Date Received"
		PayPal_Transferred_To_Bank as "Transferred To Bank"
		First_Name as "First Name"
		Last_Name as "Last Name"
		Email
		Added_Time as "Added Time"
		Modified_Time as "Modified Time"
	)
	filters
	(
		"Allocation: Accommodations"  :  Donation_Type == "Reservation" || Donation_Type == "Deposit"
		"Allocation: Building Fund"  :  Fund == "Building Fund"
		"Allocation: General"  :  Fund == "General"
	)
	sort by
	(
		Added_Time descending
	)
	custom actions
	(
		"Transfer to Bank"
		(
	  			workflow = Transfer_to_Bank
				show action in view header = true
				success message = "Successfully marked donation(s) as \"Transferred to Bank\""
		)
	)
	conditional formatting
	(
		"Deposit Green"
		{
			condition = (Donation_Type == "Deposit")
			fields = [Donation_Type]
			format = "font-weight:bold;color:#1a7009;"
		}
		"General Donation"
		{
			condition = (Donation_Type == "General Donation")
			fields = [Donation_Type]
			format = "font-weight:bold;"
		}
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Force_Sync_Reservation as "Force Sync Reservation"
					Force_Sync_Reservation_Timestamp
					ID
					Donation_Type as "Donation Type"
					Fund
					Donation_Amount as "Donation Amount"
					Transaction_Fee as "Transaction Fee"
					Amount_Received as "Amount Received"
					Percentage
					Payment_Method as "Payment Method"
					Guest
					Reservation
					Conducted_Program as "Conducted Program"
					Transaction_Number as "Transaction Number"
					ZC_Payment.payment_id as "PayPal ID"
					Date_Received as "Date Received"
					PayPal_Transferred_To_Bank as "Transferred To Bank"
					First_Name as "First Name"
					Last_Name as "Last Name"
					Email
					Added_Time as "Added Time"
					Modified_Time as "Modified Time"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
			 	"Transfer to Bank"
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Force_Sync_Reservation as "Force Sync Reservation"
					Force_Sync_Reservation_Timestamp
					ID
					Donation_Type as "Donation Type"
					Fund
					Donation_Amount as "Donation Amount"
					Transaction_Fee as "Transaction Fee"
					Amount_Received as "Amount Received"
					Percentage
					Payment_Method as "Payment Method"
					Guest
					Reservation
					Conducted_Program as "Conducted Program"
					Transaction_Number as "Transaction Number"
					ZC_Payment.payment_id as "PayPal ID"
					Date_Received as "Date Received"
					PayPal_Transferred_To_Bank as "Transferred To Bank"
					First_Name as "First Name"
					Last_Name as "Last Name"
					Email
					Added_Time as "Added Time"
					Modified_Time as "Modified Time"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
    	 	)
    	)
	)
}
