list Donations_Duplicates
{
	displayName = "Possible Duplicates"
	show all rows from Donation  [Possible_Duplicate]  
	(
		ID
		(
			width = 0px
		)
		Guest
		(
			width = 200px
			view = Guest_Info_Rpt
		)
		Donation_Type as "Type"
		(
			width = 131px
		)
		Fund
		Donation_Amount as "Amount"
		(
			width = 0px
		)
		Payment_Method as "Method"
		(
			width = 0px
		)
		Reservation
		(
			width = 200px
			view = Reservations_View_Edit
		)
		Conducted_Program as "CPm"
		(
			view = CP_CurrentYear
		)
		TPL_Number as "TPL #"
		(
			width = 0px
		)
		TPL_File as "TPL File"
		ZC_Payment.payment_id as "Payment"
		(
			view = Paypal_WPS_History_View
		)
		Payment_Status as "Payment Status"
		(
			width = 0px
		)
		Transaction_Number as "Transaction #"
		First_Name as "First"
		(
			width = 0px
		)
		Last_Name as "Last"
		(
			width = 0px
		)
		Email
		(
			width = 145px
		)
		Date_Received as "Received"
		(
			width = 0px
		)
		Added_Time as "Added"
		(
			width = 126px
		)
		Modified_Time as "Modified"
		Accountant_Notified as "Account Notified"
		Administrator_Notified as "Admin Notified"
		TPL_Thank_You_Required as "TPL Thank You Required"
		TPL_Thank_You_Sent as "TPL Thank You Sent"
		Token as "Security Token"
		Possible_Duplicate as "Possible Dupe"
		custom action "Not a Duplicate"
		(
	  			workflow = Not_a_Duplicate
				show action in view header = true
				show action in edit record menu = true
				column header = "Not a Duplicate"
				success message = "Successfully marked donation as \"Not a Duplicate\""
		)
		custom action "Delete"
		(
	  			workflow = Delete
				show action in edit record menu = true
				column header = "Delete"
				success message = "Successfully deleted donation record"
		)
	)
	sort by
	(
		Guest ascending
		Donation_Amount ascending
		Payment_Method ascending
		Added_Time ascending
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					ID
					Guest
					Donation_Type as "Type"
					Fund
					Donation_Amount as "Amount"
					Payment_Method as "Method"
					Reservation
					Conducted_Program as "CPm"
					TPL_Number as "TPL #"
					TPL_File as "TPL File"
					ZC_Payment.payment_id as "Payment"
					Payment_Status as "Payment Status"
					Transaction_Number as "Transaction #"
					First_Name as "First"
					Last_Name as "Last"
					Email
					Date_Received as "Received"
					Added_Time as "Added"
					Modified_Time as "Modified"
					Accountant_Notified as "Account Notified"
					Administrator_Notified as "Admin Notified"
					TPL_Thank_You_Required as "TPL Thank You Required"
					TPL_Thank_You_Sent as "TPL Thank You Sent"
					Token as "Security Token"
					Possible_Duplicate as "Possible Dupe"
				"Not a Duplicate" as "Not a Dupe"
				"Delete"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
			 	"Not a Duplicate"
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
				"Not a Duplicate"
				"Delete"
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
				"Not a Duplicate"
				"Delete"
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					ID
					Guest
					Donation_Type as "Type"
					Fund
					Donation_Amount as "Amount"
					Payment_Method as "Method"
					Reservation
					Conducted_Program as "CPm"
					TPL_Number as "TPL #"
					TPL_File as "TPL File"
					ZC_Payment.payment_id as "Payment"
					Payment_Status as "Payment Status"
					Transaction_Number as "Transaction #"
					First_Name as "First"
					Last_Name as "Last"
					Email
					Date_Received as "Received"
					Added_Time as "Added"
					Modified_Time as "Modified"
					Accountant_Notified as "Account Notified"
					Administrator_Notified as "Admin Notified"
					TPL_Thank_You_Required as "TPL Thank You Required"
					TPL_Thank_You_Sent as "TPL Thank You Sent"
					Token as "Security Token"
					Possible_Duplicate as "Possible Dupe"
				"Not a Duplicate" as "Not a Dupe"
				"Delete"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
			 	"Not a Duplicate" as "Not a Dupe"
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
			 	"Delete"
    	 	)
    	)
	)
}
