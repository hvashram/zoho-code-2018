list TPL_Thank_You
{
	displayName = "TPL Thank You"
   	hide = true
	show all rows from Donation    
	(
		ID
		Donation_Type as "Donation Type"
		Donation_Amount as "Donation Amount"
		Payment_Method as "Payment Method"
		Guest
		First_Name as "First Name"
		Last_Name as "Last Name"
		Email
		Reservation
		Other_Reservations as "Other Reservations"
		Conducted_Program as "Conducted Program"
		Fund
		Date_Received as "Date Received"
		TPL_Number as "TPL Number"
		TPL_File as "TPL File"
		Token as "Security Token"
		Added_Time as "Added Time"
		Guest.First_Name as "Guest_First Name"
		Guest.Last_Name as "Guest_Last Name"
		Guest.Email as "Guest_Email"
		Guest.Address_1 as "Guest_Address 1"
		Guest.Address_2 as "Guest_Address 2"
		Guest.City as "Guest_City"
		Guest.State_New as "Guest_State"
		Guest.Region as "Guest_Region"
		Guest.Country_Enter as "Country"
		Guest.Zip_Country_Code as "Guest_Postal Code"
		Guest.Phone_Home as "Guest_Phone Home"
		Guest.Phone_Mobile as "Guest_Phone Mobile"
		Guest.Phone_Work as "Guest_Phone Work"
		Reservation.From_Date as "Reservation_From Date"
		Reservation.To_Date as "Reservation_To Date"
		Reservation.Guest_Type as "Reservation_Guest Type"
		Reservation.RoomNbr as "Reservation_Room Number"
		Reservation.Room_Status as "Reservation_Room Status"
		Reservation.Number_of_Total_Days as "Reservation_Length of Stay"
		Reservation.Suggested_Donation as "Reservation_Suggested Donation"
		Reservation.Donation_Received as "Reservation_Donation Received"
		Conducted_Program.CP_Year as "CP_CP Year"
		Conducted_Program.CP_Month as "CP_CP Month"
		Conducted_Program.Program_Name as "CP_Program Name"
		Conducted_Program.From_Date as "CP_From Date"
		Conducted_Program.To_Date as "CP_To Date"
	)
	filters
	(
		Donation_Type
		Fund
		Payment_Method
	)
	sort by
	(
		Added_Time descending
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					ID
					Donation_Type as "Donation Type"
					Donation_Amount as "Donation Amount"
					Payment_Method as "Payment Method"
					Guest
					First_Name as "First Name"
					Last_Name as "Last Name"
					Email
					Reservation
					Other_Reservations as "Other Reservations"
					Conducted_Program as "Conducted Program"
					Fund
					Date_Received as "Date Received"
					TPL_Number as "TPL Number"
					TPL_File as "TPL File"
					Token as "Security Token"
					Added_Time as "Added Time"
					Guest.First_Name as "Guest_First Name"
					Guest.Last_Name as "Guest_Last Name"
					Guest.Email as "Guest_Email"
					Guest.Address_1 as "Guest_Address 1"
					Guest.Address_2 as "Guest_Address 2"
					Guest.City as "Guest_City"
					Guest.State_New as "Guest_State"
					Guest.Region as "Guest_Region"
					Guest.Country_Enter as "Country"
					Guest.Zip_Country_Code as "Guest_Postal Code"
					Guest.Phone_Home as "Guest_Phone Home"
					Guest.Phone_Mobile as "Guest_Phone Mobile"
					Guest.Phone_Work as "Guest_Phone Work"
					Reservation.From_Date as "Reservation_From Date"
					Reservation.To_Date as "Reservation_To Date"
					Reservation.Guest_Type as "Reservation_Guest Type"
					Reservation.RoomNbr as "Reservation_Room Number"
					Reservation.Room_Status as "Reservation_Room Status"
					Reservation.Number_of_Total_Days as "Reservation_Length of Stay"
					Reservation.Suggested_Donation as "Reservation_Suggested Donation"
					Reservation.Donation_Received as "Reservation_Donation Received"
					Conducted_Program.CP_Year as "CP_CP Year"
					Conducted_Program.CP_Month as "CP_CP Month"
					Conducted_Program.Program_Name as "CP_Program Name"
					Conducted_Program.From_Date as "CP_From Date"
					Conducted_Program.To_Date as "CP_To Date"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					ID
					Donation_Type as "Donation Type"
					Donation_Amount as "Donation Amount"
					Payment_Method as "Payment Method"
					Guest
					First_Name as "First Name"
					Last_Name as "Last Name"
					Email
					Reservation
					Other_Reservations as "Other Reservations"
					Conducted_Program as "Conducted Program"
					Fund
					Date_Received as "Date Received"
					TPL_Number as "TPL Number"
					TPL_File as "TPL File"
					Token as "Security Token"
					Added_Time as "Added Time"
					Guest.First_Name as "Guest_First Name"
					Guest.Last_Name as "Guest_Last Name"
					Guest.Email as "Guest_Email"
					Guest.Address_1 as "Guest_Address 1"
					Guest.Address_2 as "Guest_Address 2"
					Guest.City as "Guest_City"
					Guest.State_New as "Guest_State"
					Guest.Region as "Guest_Region"
					Guest.Country_Enter as "Country"
					Guest.Zip_Country_Code as "Guest_Postal Code"
					Guest.Phone_Home as "Guest_Phone Home"
					Guest.Phone_Mobile as "Guest_Phone Mobile"
					Guest.Phone_Work as "Guest_Phone Work"
					Reservation.From_Date as "Reservation_From Date"
					Reservation.To_Date as "Reservation_To Date"
					Reservation.Guest_Type as "Reservation_Guest Type"
					Reservation.RoomNbr as "Reservation_Room Number"
					Reservation.Room_Status as "Reservation_Room Status"
					Reservation.Number_of_Total_Days as "Reservation_Length of Stay"
					Reservation.Suggested_Donation as "Reservation_Suggested Donation"
					Reservation.Donation_Received as "Reservation_Donation Received"
					Conducted_Program.CP_Year as "CP_CP Year"
					Conducted_Program.CP_Month as "CP_CP Month"
					Conducted_Program.Program_Name as "CP_Program Name"
					Conducted_Program.From_Date as "CP_From Date"
					Conducted_Program.To_Date as "CP_To Date"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
    	 	)
    	)
	)
}
