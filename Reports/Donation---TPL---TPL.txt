list TPL
{
	displayName = "TPL"
   	hide = true
	show all rows from Donation  [Administrator_Notified is not null && TPL_Thank_You_Sent is null && TPL_Number != ""]  
	(
		TPL_Number as "TPL Number"
		TPL_File as "TPL File"
	)
	sort by
	(
		TPL_Number ascending
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					TPL_Number as "TPL Number"
					TPL_File as "TPL File"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					TPL_Number as "TPL Number"
					TPL_File as "TPL File"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
    	 	)
    	)
	)
}
