list th_Qtr_Rpt
{
	displayName = "4thQtr Rpt"
   	hide = true
	show all rows from Fourth_Qtr    
	(
		Position
		FirstLast
		Start_Date_Time
		Start_Date
		Start_Time
		End_Date_Time
		End_Date
		End_Time
		Duration
		(
			display = total	
		)
		Last_Name as "Last Name"
		From_Date as "From Date"
		To_Date as "To Date"
		Ashram_Depts_Locations as "Ashram Depts / Locations"
		Department_Name as "Department Name"
		Location
		Training
		Res_Guest_Type
		Res_Remain_Days
		(
			display = total	
		)
		NotHere
		Ashram_Quarterly_Duty_Schedule as "Ashram Quarterly Duty Schedule"
		Day_of_Week_Fx as "Day of Week Fx"
		(
			display = total	
		)
		DayOfWeek
		Reservation_Form as "Reservation Form"
		Assigned_To as "Reservation Form"
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Position
					FirstLast
					Start_Date_Time
					Start_Date
					Start_Time
					End_Date_Time
					End_Date
					End_Time
					Duration
					Last_Name as "Last Name"
					From_Date as "From Date"
					To_Date as "To Date"
					Ashram_Depts_Locations as "Ashram Depts / Locations"
					Department_Name as "Department Name"
					Location
					Training
					Res_Guest_Type
					Res_Remain_Days
					NotHere
					Ashram_Quarterly_Duty_Schedule as "Ashram Quarterly Duty Schedule"
					Day_of_Week_Fx as "Day of Week Fx"
					DayOfWeek
					Reservation_Form as "Reservation Form"
					Assigned_To as "Reservation Form"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Position
					FirstLast
					Start_Date_Time
					Start_Date
					Start_Time
					End_Date_Time
					End_Date
					End_Time
					Duration
					Last_Name as "Last Name"
					From_Date as "From Date"
					To_Date as "To Date"
					Ashram_Depts_Locations as "Ashram Depts / Locations"
					Department_Name as "Department Name"
					Location
					Training
					Res_Guest_Type
					Res_Remain_Days
					NotHere
					Ashram_Quarterly_Duty_Schedule as "Ashram Quarterly Duty Schedule"
					Day_of_Week_Fx as "Day of Week Fx"
					DayOfWeek
					Reservation_Form as "Reservation Form"
					Assigned_To as "Reservation Form"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
    	 	)
    	)
	)
}
