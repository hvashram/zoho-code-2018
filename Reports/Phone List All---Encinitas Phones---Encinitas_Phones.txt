list Encinitas_Phones
{
	displayName = "Encinitas Phones"
	show all rows from Phone_List_All1  [Location == "Encinitas Ashram Center" && Location is not null]  
	(
		Title
		Name
		Type
		Work
		Room
		VoiceMail
		Email
		Location
		FaxExt
		FaxNumber
	)
	group by
	(
		Location ascending
		Type ascending
	)
	sort by
	(
		Name ascending
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Title
					Name
					Type
					Work
					Room
					VoiceMail
					Email
					Location
					FaxExt
					FaxNumber
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Title
					Name
					Type
					Work
					Room
					VoiceMail
					Email
					Location
					FaxExt
					FaxNumber
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Print 
    	 	)
    	)
	)
}
