list Monks_Residents_Employees_Alpha
{
	displayName = "Monks/Residents/Employees Alpha"
	show all rows from Phone_List_All1  [Type == "Monk" || Type == "Resident" || Type == "Resident Emp" || Type == "Emp/Vol" && Location is not null]  
	(
		Name
		Title
		Email
		Location
		Work
		Room
		Type
		VoiceMail
		FaxExt
		FaxNumber
		Location2
	)
	sort by
	(
		Name ascending
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Name
					Title
					Email
					Location
					Work
					Room
					Type
					VoiceMail
					FaxExt
					FaxNumber
					Location2
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Name
					Title
					Email
					Location
					Work
					Room
					Type
					VoiceMail
					FaxExt
					FaxNumber
					Location2
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Print 
    	 	)
    	)
	)
}
