list Monks_Only_Phones
{
	displayName = "Monks_Only_Phones"
	show all rows from Phone_List_All1  [Type == "Monk"]  
	(
		Title
		Name
		Email
		AssignedTo
		Location
		Location2
		Type
		Work
		Room
		Other__Ph as "Other_Ph"
		VoiceMail
		FaxExt
		Radio
		FaxNumber
	)
	filters
	(
		Location
	)
	group by
	(
		properties
    	(
  	    	display record count = true
   		)
		Type ascending
	)
	sort by
	(
		Name ascending
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Title
					Name
					Email
					AssignedTo
					Location
					Location2
					Type
					Work
					Room
					Other__Ph as "Other_Ph"
					VoiceMail
					FaxExt
					Radio
					FaxNumber
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Title
					Name
					Email
					AssignedTo
					Location
					Location2
					Type
					Work
					Room
					Other__Ph as "Other_Ph"
					VoiceMail
					FaxExt
					Radio
					FaxNumber
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Print 
    	 	)
    	)
	)
}
