list Departing_List_by_Cabin
{
	displayName = "Departing List by Cabin"
   	hide = true
	show all rows from Reservation_Form  [Room_Status == "Checked Out" || Room_Status == "Occupied" || Room_Status == "Confirmed" && To_Date in next 3 days]  
	(
		Check_Room as "OK to Work"
		RoomNbr.Dirty
		To_Date as "Departing"
		From_Date as "Arriving"
		RoomNbr.Room_Number as "Room"
		(
			width = 20px
		)
		Guest_First_Name as "First Name"
		Guest_Last_Name as "Last Name"
		Guest_Type as "Guest Type"
		Attending_CP as "CP?"
		RoomNbr as "Room Number"
		(
			view = Room
		)
		Room_Status as "Room Status"
		Number_of_Total_Days as "# Days"
		(
			width = 30px
		)
		RoomNbr.Room_Location as "Room Location"
	)
	filters
	(
		Room_Status
		Conducted_Programs_Info
		Guest_Type
		From_Date
		To_Date
		"Arriving This Week"  :  From_Date in next 7 days || From_Date == today && Room_Status == "Confirmed"
		"Arriving next 2 Weeks"  :  From_Date in next 2 weeks
		"Entered/Modified Today"  :  Added_Time == today || Modified_Time == today
		"Entered Modified Last 10- Days"  :  Added_Time in last 10 days || Added_Time == today || Modified_Time in last 10 days || Modified_Time == today
	)
	group by
	(
		properties
    	(
  	    	display record count = true
   		)
		To_Date ascending
		RoomNbr.Room_Location ascending
	)
	sort by
	(
		RoomNbr.Room_Number ascending
	)
	conditional formatting
	(
		"ClosedForMaint RED"
		{
			condition = (Guest_Type == "Closed for Maintenance")
			fields = [From_Date, RoomNbr.Room_Number, Room_Status]
			format = "font-weight:bold;color:#ff0000;"
		}
		"CP True BLUE"
		{
			condition = (Attending_CP)
			fields = [Attending_CP]
			format = "font-weight:bold;color:#2d00e3;"
		}
		"CP False GrayedOut"
		{
			condition = (Attending_CP == false)
			fields = [Attending_CP]
			format = "color:#d4d4d4;"
		}
		"Cancelled RED"
		{
			condition = (Room_Status == "Cancelled")
			fields = [Check_Room, RoomNbr.Dirty, To_Date, Guest_First_Name, Guest_Last_Name, Guest_Type, Room_Status, Number_of_Total_Days]
			format = "font-weight:bold;color:#ebb7b7;"
		}
		"Occupied Blue"
		{
			condition = (Room_Status == "Occupied")
			fields = [From_Date, To_Date, RoomNbr.Room_Number, Guest_First_Name, Guest_Last_Name, Guest_Type, Room_Status, Number_of_Total_Days]
			format = "font-weight:bold;color:#a69cf0;"
		}
		"Arriving Next 10 Days BOLD"
		{
			condition = (From_Date in next 10 days)
			fields = [From_Date, RoomNbr.Room_Number, Guest_First_Name, Guest_Last_Name, Guest_Type, Room_Status]
			format = "font-weight:bold;color:#141414;background-color:#ffffff;"
		}
		"DIRTY RED"
		{
			condition = (RoomNbr.Dirty)
			fields = [RoomNbr.Dirty]
			format = "font-weight:bold;color:#ff0000;"
		}
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Check_Room as "OK to Work"
					RoomNbr.Dirty as "Dirty"
					To_Date as "Departing"
					From_Date as "Arriving"
					RoomNbr.Room_Number as "Room"
					Guest_First_Name as "First Name"
					Guest_Last_Name as "Last Name"
					Guest_Type as "Guest Type"
					Attending_CP as "CP?"
					RoomNbr as "Room Number"
					Room_Status as "Room Status"
					Number_of_Total_Days as "# Days"
					RoomNbr.Room_Location as "Room Location"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Add 
    	 	)
    	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Check_Room as "OK to Work"
					RoomNbr.Dirty as "Dirty"
					To_Date as "Departing"
					From_Date as "Arriving"
					RoomNbr.Room_Number as "Room"
					Guest_First_Name as "First Name"
					Guest_Last_Name as "Last Name"
					Guest_Type as "Guest Type"
					Attending_CP as "CP?"
					RoomNbr as "Room Number"
					Room_Status as "Room Status"
					Number_of_Total_Days as "# Days"
					RoomNbr.Room_Location as "Room Location"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Delete 
    		 	Duplicate 
    		 	Print 
    	 	)
    	)
	)
}
