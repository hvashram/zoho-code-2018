list Envelope
{
	displayName = "Envelope"
   	hide = true
	show all rows from Reservation_Form    
	(
		Guest_Info as "Guest Info"
		(
			view = Guest_Info_Rpt
		)
		From_Date as "From Date"
		To_Date as "To Date"
		RoomNbr as "Room Number"
		Guest_Info.Email
		Guest_Info.Last_Name as "Last Name"
		Guest_Info.Address_1 as "Address 1"
		Guest_Info.Address_2 as "Address 2"
		Guest_Info.City
		Guest_Info.State_New as "State"
		Guest_Info.Region
		Guest_Info.Phone_Mobile as "Phone Mobile"
	)
	filters
	(
		"Arriving Today"  :  From_Date == today
		"Next 2 Days"  :  From_Date in next 2 days
		"Next 3 Days"  :  From_Date in next 3 days
		"Next 4 days"  :  From_Date in next 4 days
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Guest_Info as "Guest Info"
					From_Date as "From Date"
					To_Date as "To Date"
					RoomNbr as "Room Number"
					Guest_Info.Email as "Email"
					Guest_Info.Last_Name as "Last Name"
					Guest_Info.Address_1 as "Address 1"
					Guest_Info.Address_2 as "Address 2"
					Guest_Info.City as "City"
					Guest_Info.State_New as "State"
					Guest_Info.Region as "Region"
					Guest_Info.Phone_Mobile as "Phone Mobile"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Guest_Info as "Guest Info"
					From_Date as "From Date"
					To_Date as "To Date"
					RoomNbr as "Room Number"
					Guest_Info.Email as "Email"
					Guest_Info.Last_Name as "Last Name"
					Guest_Info.Address_1 as "Address 1"
					Guest_Info.Address_2 as "Address 2"
					Guest_Info.City as "City"
					Guest_Info.State_New as "State"
					Guest_Info.Region as "Region"
					Guest_Info.Phone_Mobile as "Phone Mobile"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
    	 	)
    	)
	)
}
