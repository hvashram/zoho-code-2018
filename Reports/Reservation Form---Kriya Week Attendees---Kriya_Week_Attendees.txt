list Kriya_Week_Attendees
{
	displayName = "Kriya Week Attendees"
	show all rows from Reservation_Form  [Program_attendance_status == "Approved" && Conducted_Programs_Info.Program_Name.contains("kriya") && !FirstLast.contains("BLOCKED") && From_Date < zoho.currentdate]  
	(
		Conducted_Programs_Info as "Conducted Program Name"
		Program_attendance_status as "CP Status"
		Guest_First_Name as "First Name"
		Guest_Last_Name as "Last Name"
		Email
		Deposit
		CP_Deposit
		Donation_Received as "Donation Received"
		Donation as "Paid?"
		Donation_Amount as "Actual"
		Suggested_Donation as "Suggested"
		Room_Status as "Room Status"
		Waiting_Priority as "Priority"
		Guest_Type as "Guest Type"
		Modified_Time as "Modified Time"
		Conducted_Programs_Info.CP_Year as "CP Year"
		custom action "Add Deposit"
		(
	  			workflow = Add_Deposit2
				show action in edit record menu = true
				column header = "Add Deposit"
				condition = (Deposit == "Unpaid")
		)
		custom action "AddDONATION"
		(
	  			workflow = Add_DONATION1
				show action in edit record menu = true
				column header = "AddDONATION"
				condition = (Donation == "Unpaid")
		)
		Guest_Info as "Guest Info"
		Assigned_Counselor as "Assigned Counselor"
		RoomNbr.Room_Number as "Room #"
		From_Date as "Arrival"
		Attending_CP as "Is this Reservation for a Conducted Program?"
		To_Date as "Departure"
		Conducted_Programs_Info.CP_Month as "CP Month"
		FirstLast
	)
	filters
	(
		"Remove Approved"  :  Program_attendance_status != "Approved"
	)
	sort by
	(
		FirstLast ascending
	)
	conditional formatting
	(
		"WAITING"
		{
			condition = (Program_attendance_status == "Waiting")
			fields = [Email, Guest_First_Name, Guest_Last_Name, Program_attendance_status, Donation_Received]
			format = "font-weight:bold;"
		}
		"Approved"
		{
			condition = (Program_attendance_status == "Approved")
			fields = [Email, Guest_First_Name, Guest_Last_Name, Program_attendance_status, CP_Deposit, Deposit, Donation_Received]
			format = "text-decoration:line-through;color:#cbcbcb;"
		}
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					FirstLast
					Conducted_Programs_Info as "Conducted Program Name"
					From_Date as "Arrival"
					To_Date as "Departure"
					Program_attendance_status as "CP Status"
					Guest_First_Name as "First Name"
					Guest_Last_Name as "Last Name"
					Email
					Deposit
					Donation_Received as "Donation Received"
					Donation as "Paid?"
					Donation_Amount as "Actual"
					Suggested_Donation as "Suggested"
					Room_Status as "Room Status"
					Waiting_Priority as "Priority"
					Guest_Type as "Guest Type"
					Modified_Time as "Modified Time"
					Conducted_Programs_Info.CP_Month as "CP Month"
					Conducted_Programs_Info.CP_Year as "CP Year"
					Guest_Info as "Guest Info"
					Assigned_Counselor as "Assigned Counselor"
					RoomNbr.Room_Number as "Room #"
					Attending_CP as "Is this Reservation for a Conducted Program?"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
				"Add Deposit"
				"AddDONATION" as "Add DONATION"
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
				"Add Deposit"
				"AddDONATION" as "Add DONATION"
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Conducted_Programs_Info as "Conducted Program Name"
					Program_attendance_status as "CP Status"
					Guest_First_Name as "First Name"
					Guest_Last_Name as "Last Name"
					Email
					Deposit
					CP_Deposit
					Donation_Received as "Donation Received"
					Donation as "Paid?"
					Donation_Amount as "Actual"
					Suggested_Donation as "Suggested"
					Room_Status as "Room Status"
					Waiting_Priority as "Priority"
					Guest_Type as "Guest Type"
					Modified_Time as "Modified Time"
					Conducted_Programs_Info.CP_Year as "CP Year"
				"Add Deposit"
				"AddDONATION"
					Guest_Info as "Guest Info"
					Assigned_Counselor as "Assigned Counselor"
					RoomNbr.Room_Number as "Room #"
					From_Date as "Arrival"
					Attending_CP as "Is this Reservation for a Conducted Program?"
					To_Date as "Departure"
					Conducted_Programs_Info.CP_Month as "CP Month"
					To_Date as "Departure"
					Modified_Time as "Modified Time"
					Conducted_Programs_Info.CP_Month as "CP Month"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
			 	"Add Deposit"
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
			 	"AddDONATION"
    	 	)
    	)
	)
}
