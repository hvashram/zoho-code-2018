list Residents_Occupied_Confirmed
{
	displayName = "Residents Occupied_Confirmed"
	show all rows from Reservation_Form  [Guest_Type == "Resident" && Room_Status == "Occupied" || Room_Status == "Confirmed"]  
	(
		Guest_First_Name as "First Name"
		Guest_Last_Name as "Last Name"
		From_Date as "From Date"
		To_Date as "To Date"
		Long_Term_Reservation
		Number_of_Total_Days as "Number of total days"
		RoomNbr as "Room Number"
		Room_Status as "Room Status"
		Guest_Type as "Guest Type"
		Days_Remaining
		Get_Year as "Get_Year_Arriving"
		Get_Month as "Get_Month_Arriving"
	)
	filters
	(
		From_Date
		To_Date
		DateTaken
		Overall_Start
		Overall_End
	)
	sort by
	(
		To_Date ascending
	)
	conditional formatting
	(
		"Occupied"
		{
			condition = (Room_Status == "Occupied")
			fields = [Guest_First_Name, From_Date, To_Date, Room_Status, Guest_Last_Name]
			format = "font-weight:bold;color:#0000ff;"
		}
		"ToDate"
		{
			condition = (To_Date in next 30 days)
			fields = [Guest_First_Name, From_Date, To_Date, RoomNbr, Room_Status, Guest_Last_Name, Guest_Type, Number_of_Total_Days, Long_Term_Reservation, Days_Remaining, Get_Year, Get_Month]
			format = "font-weight:bold;color:#fc0505;"
		}
		"Confirmed"
		{
			condition = (Room_Status == "Confirmed")
			fields = [Guest_First_Name, From_Date, To_Date, RoomNbr, Room_Status, Guest_Last_Name, Guest_Type, Number_of_Total_Days, Days_Remaining, Get_Year, Get_Month]
			format = "color:#10c762;"
		}
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Guest_First_Name as "First Name"
					Guest_Last_Name as "Last Name"
					From_Date as "From Date"
					To_Date as "To Date"
					Long_Term_Reservation
					Number_of_Total_Days as "Number of total days"
					RoomNbr as "Room Number"
					Room_Status as "Room Status"
					Guest_Type as "Guest Type"
					Days_Remaining
					Get_Year as "Get_Year_Arriving"
					Get_Month as "Get_Month_Arriving"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    		 	Add 
    		 	Print 
    		 	Import 
    		 	Export 
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Print   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				Print   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Guest_First_Name as "First Name"
					Guest_Last_Name as "Last Name"
					From_Date as "From Date"
					To_Date as "To Date"
					Long_Term_Reservation
					Number_of_Total_Days as "Number of total days"
					RoomNbr as "Room Number"
					Room_Status as "Room Status"
					Guest_Type as "Guest Type"
					Days_Remaining
					Get_Year as "Get_Year_Arriving"
					Get_Month as "Get_Month_Arriving"
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Print 
    		 	Delete 
    	 	)
    	)
	)
}
