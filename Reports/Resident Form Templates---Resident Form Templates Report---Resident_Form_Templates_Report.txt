list Resident_Form_Templates_Report
{
	displayName = "Resident Form Templates Report"
	show all rows from Resident_Form_Templates    
	(
		Form_Name as "Form Name"
		FormText
	)
	quickview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Form_Name as "Form Name"
					FormText
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    	 	)
			record
			(
				Edit   	   
				Duplicate   	   
				Delete   	   
    		)
    	)
    	action
    	(
			on click
			(
				View Record   	   
    		)
			on right click
			(
				Edit   	   
				Delete   	   
				Duplicate   	   
				View Record   	   
    		)
     	)
	)
	detailview
	(
		layout
		(
			datablock1
			(
		     	title = "Overview"
				fields
				(
					Form_Name as "Form Name"
					FormText
				)
			)
		)

		menu
    	(
    	 	header
    	 	(
    		 	Edit 
    		 	Duplicate 
    		 	Delete 
    	 	)
    	)
	)
}
