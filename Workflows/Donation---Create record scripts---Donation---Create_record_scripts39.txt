Create_record_scripts39 as "Create record scripts"
{
    type =  form
	form = Donation
	on add
	{
		actions 
		{
			on validate
			(
				// 'Guest' field is mandatory if the donation type is Reservation or Deposit
				if(input.Donation_Type == "Reservation" || input.Donation_Type == "Deposit")
				{
					if(isNull(input.Guest))
					{
						alert "Please select the <u>Guest</u> that this donation is coming from.";
						cancel submit;
					}
				}
				// 'Guest' field is mandatory if the payment method is PayPal, Credit Card, or Check
				if(input.Payment_Method == "PayPal" || input.Payment_Method == "Credit Card" || input.Payment_Method == "Check")
				{
					if(zoho.loginuser != "Public" && isNull(input.Guest))
					{
						alert "Please select the <u>Guest</u> that this donation is coming from.";
						cancel submit;
					}
				}
				// 'Reservation' field is mandatory if the donation type is Reservation
				if(input.Donation_Type == "Reservation" && isNull(input.Reservation.ID))
				{
					alert "Please select the <u>Reservation</u> that is being paid for with this donation.";
					cancel submit;
				}
				// 'Fund' field is mandatory if the donation type is General Donation
				if(input.Donation_Type == "General Donation")
				{
					if(isNull(input.Fund) || isBlank(input.Fund) || input.Fund == "" || input.Fund == "-Select-")
					{
						alert "Please select which <u>Fund</u> this donation should be applied to.";
						cancel submit;
					}
				}
				// When this form is being accessed publicly, and no guest is selected...
				if(zoho.loginuser == "Public" && isNull(input.Guest))
				{
					// Require First_Name, Last_Name, and Email fields
					if(isNull(input.First_Name))
					{
						alert "Please enter your <u>First Name</u>.";
						cancel submit;
					}
					if(isNull(input.Last_Name))
					{
						alert "Please enter your <u>Last Name</u>.";
						cancel submit;
					}
					if(isNull(input.Email))
					{
						alert "Please enter your <u>Email</u> address.";
						cancel submit;
					}
					// If First_Name, Last_Name, and Email have been provided, use them to attempt to lookup the Guest
					if(isNull(input.Guest) && !isNull(input.Email) && !isNull(input.First_Name) && !isNull(input.Last_Name))
					{
						// First attempt to find the guest with a combination of Email, First Name, and Last Name
						guestLookup = Guest_Info_Form[Email == input.Email.toLowerCase() && First_Name == input.First_Name.toUpperCase() && Last_Name == input.Last_Name.toUpperCase()];
						if(guestLookup.count() > 0)
						{
							input.Guest = guestLookup.ID;
						}
						else
						{
							// If we couldn't find the guest above, try searching with just the e-mail address
							guestLookup = Guest_Info_Form[Email == input.Email.toLowerCase()];
							if(guestLookup.count() > 0)
							{
								input.Guest = guestLookup.ID;
							}
						}
					}
				}
				// If this is a Reservation payment and the Reservation is part of a Conducted Program, ensure we have recorded the Conducted Program properly
				if(!isNull(input.Reservation.ID) && isNull(input.Conducted_Program.ID))
				{
					reservationRecord = Reservation_Form[ID == input.Reservation.ID];
					if(!isNull(reservationRecord.Conducted_Programs_Info))
					{
						conductedProgramID = reservationRecord.Conducted_Programs_Info;
						input.Conducted_Program = conductedProgramID;
					}
				}
			)	
			on success
			(
				// Assemble the guest info into a string we can use in notification messages
				if(!isNull(input.Guest))
				{
					// The donation is linked to a Guest record, so display Guest info from there
					guestRecord = Guest_Info_Form[ID == input.Guest];
					guestInfo = thisapp.str.capitalizeFirstLetter(guestRecord.First_Name) + " " + thisapp.str.capitalizeFirstLetter(guestRecord.Last_Name);
					if(!isNull(guestRecord.Email))
					{
						guestInfo = guestInfo + " (" + guestRecord.Email + ")";
					}
				}
				else
				{
					// There is no Guest record linked to this donation, so display the name & e-mail the user entered manually
					guestInfo = input.First_Name + " " + input.Last_Name + " (" + input.Email + ")";
				}
				// Check to see if we received a payment ID (transaction number) from PayPal/Zoho
				if(!isNull(input.ZC_Payment.payment_id))
				{
					// Copy the PayPal payment ID into our "Transaction Number" field
					input.Transaction_Number = input.ZC_Payment.payment_id;
					// Copy the PayPal transaction fee amount into the "Transaction Fee" field
					if(!isNull(input.ZC_Payment.mc_fee))
					{
						input.Transaction_Fee = input.ZC_Payment.mc_fee;
					}
					// Mark the payment "Complete"
					input.Payment_Status = "Complete";
					// Send a notification to the Front Office via SLACK
					slackMessage = "A PayPal donation has been received...";
					slackMessage = slackMessage + "\n* Date/Time: " + zoho.currenttime;
					slackMessage = slackMessage + "\n* Donation ID: " + input.ID;
					slackMessage = slackMessage + "\n* Donation Type: " + input.Donation_Type;
					slackMessage = slackMessage + "\n* Donation Amount: $" + input.Donation_Amount;
					slackMessage = slackMessage + "\n* Guest: " + guestInfo;
					if(!isNull(input.ZC_Payment.payment_id))
					{
						slackMessage = slackMessage + "\n* Payment ID: " + input.ZC_Payment.payment_id;
					}
					thisapp.slack.messageFrontOffice(slackMessage);
				}
				else
				{
					// If we did not receive a PayPal payment ID, it means the donation was not completed successfully
					// Send a message with troubleshooting info via EMAIL
					messageBody = "Someone attempted to make an online donation, but we did not receive a transaction number from PayPal, so the payment may have been unsuccessful. You should be able to find this donation record in the Donations (Incomplete) report in Zoho.<br/><br/>The details of the incomplete donation are as follows:<br/>";
					messageBody = messageBody + "<br/>* Date/Time: " + zoho.currenttime;
					messageBody = messageBody + "<br/>* Donation ID: " + input.ID;
					messageBody = messageBody + "<br/>* Donation Type: " + input.Donation_Type;
					messageBody = messageBody + "<br/>* Donation Amount: $" + input.Donation_Amount;
					messageBody = messageBody + "<br/>* Payment Status: " + input.Payment_Status;
					messageBody = messageBody + "<br/>* Guest Record: " + input.Guest;
					messageBody = messageBody + "<br/>* Guest: " + guestInfo;
					messageBody = messageBody + "<br/>* Reservation: " + input.Reservation;
					messageBody = messageBody + "<br/>* Payment Processing Status (from Zoho): " + input.ZC_Payment.payment_processing_status;
					messageBody = messageBody + "<br/>* Payment Status (from PayPal): " + input.ZC_Payment.payment_status;
					messageBody = messageBody + "<br/>* Payment Type (from PayPal): " + input.ZC_Payment.payment_type;
					messageBody = messageBody + "<br/>* Payer Status (from PayPal): " + input.ZC_Payment.payer_status;
					messageBody = messageBody + "<br/>* Payer First Name (from PayPal): " + input.ZC_Payment.first_name;
					messageBody = messageBody + "<br/>* Payer Last Name (from PayPal): " + input.ZC_Payment.last_name;
					messageBody = messageBody + "<br/>* Payer Email (from PayPal): " + input.ZC_Payment.payer_email;
					messageBody = messageBody + "<br/>* Payer ID (from PayPal): " + input.ZC_Payment.payer_id;
					messageBody = messageBody + "<br/>* Transaction Type (from PayPal): " + input.ZC_Payment.txn_type;
					messageBody = messageBody + "<br/>* Transaction ID (from PayPal): " + input.ZC_Payment.txn_id;
					messageBody = messageBody + "<br/><br/>All parameters received from PayPal:<br/><br/>" + input.ZC_Payment.all_params;
					sendmail
					[
						from :zoho.adminuserid
						to :"Francis Collins <mfrancis@hvashram.org>"
						bcc :"Peter Boudreau <peter.boudreau@hvashram.org>"
						subject :"[Zoho] INCOMPLETE DONATION"
						message :messageBody
					]
					// Send a message with troubleshooting info via SLACK
					messageBody = "Someone attempted to make an online donation, but we did not receive a transaction number from PayPal, so the payment may have been unsuccessful. You should be able to find this donation record in the Donations (Incomplete) report in Zoho.\n\nThe details of the incomplete donation are as follows:\n";
					messageBody = messageBody + "\n* Date/Time: " + zoho.currenttime;
					messageBody = messageBody + "\n* Donation ID: " + input.ID;
					messageBody = messageBody + "\n* Donation Type: " + input.Donation_Type;
					messageBody = messageBody + "\n* Donation Amount: $" + input.Donation_Amount;
					messageBody = messageBody + "\n* Payment Status: " + input.Payment_Status;
					messageBody = messageBody + "\n* Guest Record: " + input.Guest;
					messageBody = messageBody + "\n* Guest: " + guestInfo;
					messageBody = messageBody + "\n* Reservation: " + input.Reservation;
					messageBody = messageBody + "\n* Payment Processing Status (from Zoho): " + input.ZC_Payment.payment_processing_status;
					messageBody = messageBody + "\n* Payment Status (from PayPal): " + input.ZC_Payment.payment_status;
					messageBody = messageBody + "\n* Payment Type (from PayPal): " + input.ZC_Payment.payment_type;
					messageBody = messageBody + "\n* Payer Status (from PayPal): " + input.ZC_Payment.payer_status;
					messageBody = messageBody + "\n* Payer First Name (from PayPal): " + input.ZC_Payment.first_name;
					messageBody = messageBody + "\n* Payer Last Name (from PayPal): " + input.ZC_Payment.last_name;
					messageBody = messageBody + "\n* Payer Email (from PayPal): " + input.ZC_Payment.payer_email;
					messageBody = messageBody + "\n* Payer ID (from PayPal): " + input.ZC_Payment.payer_id;
					messageBody = messageBody + "\n* Transaction Type (from PayPal): " + input.ZC_Payment.txn_type;
					messageBody = messageBody + "\n* Transaction ID (from PayPal): " + input.ZC_Payment.txn_id;
					messageBody = messageBody + "\n\nAll parameters received from PayPal:\n\n" + input.ZC_Payment.all_params;
					thisapp.slack.messageFrontOffice(messageBody);
				}
				// Calculate the "Amount Received" by subtracting the "Transaction Fee" from the "Donation Amount"
				input.Amount_Received = ifnull(input.Donation_Amount,0.0) - ifnull(input.Transaction_Fee,0.0);
				if(!isNull(input.Reservation))
				{
					// If this donation is for a reservation, update the donation information on the reservation record
					// thisapp.payments.syncDonationToReservation(input.Reservation);
					// ** Peter's Notes (2015-11-13): We want to simply use the line above, but we can't because of a bug in Zoho Creator.
					// ** The bug is that, for whatever reason, we can't update records from the 'Add - On Success' event when returning from a PayPal payment (we can read records just fine).
					// ** Calling the above function (syncDonationToReservation) from anywhere else will work fine.
					// ** So as an absolutely ridiculous workaround (below), we're making a call to the Zoho Creator API to accomplish our goal.
					// ** The below API call makes an empty update to the 'Donation' table which triggers the 'Edit - On Success' event, which calls our syncDonationToReservation() function.
					// ** But all we want to do is simply call 'thisapp.payments.syncDonationToReservation(input.Reservation)', so if Zoho ever fixes this bug, just call the function directly.
					reservationRecord = Reservation_Form[ID == input.Reservation];
					if(reservationRecord.count() > 0)
					{
						donationRecord = Donation[ID == input.ID];
						payload = Map:String();
						payload.put("authtoken","3d4ea6530f497c3c5eda2b4cf224b58d");
						payload.put("scope","creatorapi");
						criteria = "(ID == \"" + donationRecord.ID + "\")";
						payload.put("criteria",criteria);
						response = postUrl("https://creator.zoho.com/api/mfrancishvashramorg/json/hv-reservation-app1/form/Donation/record/update/",payload);
					}
				}
				// Clear any values stored in temporary fields
				input.Temp_Reservation_ID = null;
				// Except for general donations, the "Fund" field should be left blank
				if(input.Donation_Type != "General Donation")
				{
					input.Fund = null;
				}
				// Check for any possible duplicate donations
				thisapp.payments.checkForDuplicateDonations(input.ID);
				// If the donation amount is $250 or greater, notify the accountant (the necessary checks will be performed within the function)
				thisapp.payments.notifyAccountantOfDonation(input.ID);
				// Redirect the user to our thank you page
				openUrl("https://www.hvashram.org/zoho/thank_you.php","Same window");
			)	
			on load
			(
				// For security reasons, clear the values from all dropdown fields with sensitive data or which we don't want users to see
				// (fields which are cleared here must be populated dynamically/programmatically below)
				clear Guest;
				clear Reservation;
				// Hide the fields which users don't need to see
				hide First_Name;
				hide Last_Name;
				hide Email;
				hide Fund;
				hide Reservation;
				hide Other_Reservations;
				hide Conducted_Program;
				hide CP_Notes;
				hide Donation_Amount_Notes;
				hide Transaction_Fee;
				hide Amount_Received;
				hide Payment_Status;
				hide Accountant_Notified;
				hide Administrator_Notified;
				hide TPL_Thank_You_Sent;
				hide TPL_Number;
				hide TPL_File;
				hide TPL_Thank_You_Required;
				hide PayPal_Transferred_To_Bank;
				hide Token;
				hide Possible_Duplicate;
				hide Temp_Reservation_ID;
				hide CP_Start_Date;
				hide Guest_Type;
				hide Fiscal_Year;
				hide Show_All_Reservations;
				// Set default values
				input.Date_Received = now;
				// If a security token is present, it should mean that a user is accessing this form via a link we e-mailed them
				securityTokenIsValid = false;
				if(!isNull(input.Token))
				{
					// Check if the security token in the URL is valid or not
					securityTokenIsValid = thisapp.payments.securityTokenIsValid(input.Token);
					if(securityTokenIsValid)
					{
						// Hide the fields we don't need to show the user
						hide Payment_Method;
						hide Transaction_Number;
						hide Date_Received;
						// Disable the form fields we don't want the user editing
						disable Guest;
						disable Donation_Type;
						// Get the token type from the security token
						tokenType = thisapp.payments.getTokenTypeFromToken(input.Token);
						// Determine the donation type from the token type
						donationType = if(tokenType == "Reservation" || tokenType == "Deposit",tokenType,"General Donation");
						// Fill in the Donation Type, and Payment Method fields in the form
						input.Donation_Type = donationType;
						input.Payment_Method = "PayPal";
						input.Payment_Status = "Incomplete";
						if(tokenType == "Deposit" || tokenType == "Reservation")
						{
							// Disable the form fields we don't want the user editing
							disable Reservation;
							disable Payment_Method;
							if(tokenType == "Deposit")
							{
								// We only disable Donation Amount if it's a deposit. For Reservation payments we let them choose how much.
								disable Donation_Amount;
							}
							// The reservation ID is the record ID retrieved from the security token
							reservationID = thisapp.payments.getRecordIDFromToken(input.Token);
							// Retrieve the reservation record
							reservationRecord = Reservation_Form[ID == reservationID];
							input.Guest_Type = reservationRecord.Guest_Type;
							// Retrieve the guest record using the guest ID in the reservation record
							guestRecord = Guest_Info_Form[ID == reservationRecord.Guest_Info];
							// Fill in the Guest, Reservation fields with the data we've retrieved above
							input.Guest = guestRecord.ID;
							input.Reservation = reservationRecord.ID;
							//	input.Guest_Type = reservationRecord.Guest_Type;
							// Retrieve the conducted program record using the conducted program ID in the reservation record, if there is one
							if(!isNull(reservationRecord.Conducted_Programs_Info) && reservationRecord.Conducted_Programs_Info > 0)
							{
								conductedProgramRecord = Conducted_Programs_Info_Form[ID == reservationRecord.Conducted_Programs_Info];
								input.Conducted_Program = conductedProgramRecord.ID;
							}
							// Fill in the Deposit Amount
							if(tokenType == "Deposit" && !isNull(conductedProgramRecord.Suggested_Deposit) && conductedProgramRecord.Suggested_Deposit > 0)
							{
								input.Donation_Amount = conductedProgramRecord.Suggested_Deposit;
							}
						}
						else if(tokenType == "General Donation")
						{
							// The guest ID is the record ID retrieved from the security token
							GuestID = thisapp.payments.getRecordIDFromToken(input.Token);
							// Retrieve the guest record
							guestRecord = Guest_Info_Form[ID == GuestID];
							// Fill in the Guest field with the data we've retrieved above
							input.Guest = guestRecord.ID;
						}
						else if(tokenType == "Fundraiser" || tokenType == "Anonymous Donation")
						{
							// Show the First Name, Last Name, and Email fields
							show First_Name;
							show Last_Name;
							show Email;
							// Hide the Guest and Donation Type fields
							hide Guest;
							hide Donation_Type;
						}
					}
				}
				// If this is not an authenticated Zoho user, and
				// If a security token is not present, or
				// If we weren't able to retrieve the Donation Type and Guest properly from the security token...
				// Assume this is an invalid access attempt and do not display anything
				//if ((isNull(input.Token)  ||  isNull(input.Donation_Type))  ||  isNull(input.Guest))
				if(zoho.loginuser == "Public" && isNull(input.Token) || isBlank(input.Token) || isNull(input.Donation_Type) || input.Donation_Type == "-Select-")
				{
					// Hide all remaining fields
					hide Guest;
					hide Donation_Type;
					hide Donation_Amount;
					hide Payment_Method;
					hide Transaction_Number;
					hide Date_Received;
					if(!isNull(input.Token) && !securityTokenIsValid)
					{
						// There is a security token, but it's invalid
						// Most likely the security token has expired, so redirect the user to a page explaining that
						//openUrl("https://creator.zohopublic.com/mfrancishvashramorg/hv-reservation-app1/view-perma/Security_Token_Expired/Tja9MCnadtyAsCU04skbZQ3qkRtyeJwGDskJQ7DQVqxT4BV0JFb6YJ5yBSeYFbgebwJ6YWPPbGYQJYhxxqB1BvatUKsgVyTRj7T5/", "parent window");
					}
					else
					{
						// Otherwise, it's an invalid access, so just redirect the user to a blank page, because we don't want to show them anything
						//openUrl("https://creator.zohopublic.com/mfrancishvashramorg/hv-reservation-app1/view-perma/Blank_Page/RCpyPYpvOfmVgeFT6xY5zdzUg8q5X4pWzKnE3tOBCb8HvuNPATuWHYNAHnz3UCSze485Aef14XwFPAEW92Kuu2wjSshWKW07T6YB/", "parent window");
					}
				}
			)	
		}
		actions 
		{
			on user input of Reservation
			(
				input.Guest_Type = IFNULL(input.Guest_Type,input.Reservation.Guest_Type);
			)	
		}
	}
}


