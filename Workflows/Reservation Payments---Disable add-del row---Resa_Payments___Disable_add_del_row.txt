Disable_add_del_row as "Disable add/del row"
{
    type =  form
	form = Resa_Payments
	on add or edit
	{
		actions 
		{
			hide delete row of Payment_history;
		}
	}
}


