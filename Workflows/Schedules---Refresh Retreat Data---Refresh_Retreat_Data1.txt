Refresh_Retreat_Data1 as "Refresh Retreat Data"
{
    type =  schedule
	start = "2016-02-15 04:30:00"
	frequency = daily
	on start
	{
		actions 
		{
			on load
			(
				thisapp.retreat.syncReservationToRetreat();
			)	
		}
	}
}


