Room_Updates1 as "Room Updates"
{
    type =  schedule
	start = "2015-07-29 04:00:00"
	frequency = daily
	on start
	{
		actions 
		{
			on load
			(
				thisapp.sched.reservationStatusUpdates();
				thisapp.sched.sameDay();
			)	
		}
	}
}


